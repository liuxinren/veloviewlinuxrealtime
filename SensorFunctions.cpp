#include "SensorFunctions.h"

int sockfd_;
unsigned int packet_size = 1206;
int udp_port_ = 2368;
bool playback =0;
FILE* pfile;
conf configuration;

//used to adjust the rotation value of the velodyne to correct for the fact the the velodyne turns clockwise and degrees go counter-clockwise.
double normalize_angle(double angle)
{
	double a = fmod(fmod(angle, 2.0*M_PI)+2.0*M_PI,2.0*M_PI);
	if(a> M_PI)
		a -=2.0*M_PI;
	return a;
}

//converts degrees to radians
double from_degrees(double degrees)
{
	return degrees * M_PI / 180.0;
}
/*---------------------------Parse Buffer-----------------------------
 * parses the buffer after a velodyne message has been read and places
 * the data in the proper places inside the packet struct
 * -------------------------------------------------------------------*/
int parseBuffer(uint8_t* buffer, packet* pack)
{
	//for information regarding this section please refer to appendix E: data packet format of the velodyne manual
	//also take note that all data is transmitted in big endian format
	for(int i = 0; i<12; ++i)
	{
		pack->block[i].id=((uint16_t)buffer[(i*100)+1])*256+(uint16_t)buffer[i*100]; //TODO: include check for correct id
		pack->block[i].rotation = (((double)buffer[(i*100)+3])*256+(double)buffer[i*100+2])/100.0;
		for(int j = 0; j<32; ++j)
		{
			pack->block[i].data[j].distance=(((double)buffer[(i*100)+5+3*j])*256+(double)buffer[i*100+4+3*j])*.002;
			pack->block[i].data[j].intensity= buffer[(i*100)+6+3*j];
		}
	}
	pack->time = (uint32_t)buffer[1200]+((uint32_t)buffer[1201]<<8)+((uint32_t)buffer[1202]<<16)+((uint32_t)buffer[1203]<<24);
	pack->statType = buffer[1204];
	pack->statVal=buffer[1205];
	return 0;
}

/*---------------------------Print Packet-----------------------------
 * Prints the data from the packet to the terminal so it can be viewed
 * by the user in a readable format
 * -------------------------------------------------------------------*/
void printPacket(packet pack)
{
	printf("\n------Packet Information:-----\n\n");
	printf("Time Stamp: %i\n", (int)pack.time);
	printf("Status Type: %X\n", (int)pack.statType);
	printf("Status Value: %X\n", (int)pack.statVal);
	printf("Laser Information:------\n");
	for(int i = 0; i<12; ++i)
	{
		printf("\tBlock Number: %i\n", i);
		printf("\tID: %X\n", (int)pack.block[i].id);
		printf("\tRotation: %f\n", pack.block[i].rotation);
		printf("\tBlock Data:------\n");
		for(int j = 0; j<32; ++j)
		{
			printf("\t\tLaser: %i\n", j);
			printf("\t\tDistance: %f\n", pack.block[i].data[j].distance);
			printf("\t\tIntensity: %i\n", (int)pack.block[i].data[j].intensity);
		}
	}
	printf("\n------End Packet-----------\n\n");
}

/*---------------------------Velodyne Open-----------------------------
 * Opens the UDP Socket so that messages can be read
 * -------------------------------------------------------------------*/
int vopen(void)
{
	//open a datagram socket
    sockfd_ = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockfd_ == -1)
      {
        printf("socket error\n");
        return -1;
      }
    printf("socket open\n");
    sockaddr_in my_addr;                     // my address information
    memset(&my_addr, 0, sizeof(my_addr));    // initialize to zeros
    my_addr.sin_family = AF_INET;            // host byte order
    my_addr.sin_port = htons(udp_port_);     // short, in network byte order
    my_addr.sin_addr.s_addr = INADDR_ANY;    // automatically fill in my IP
    //bind the socket to the port and ip address that the velodyne transers data over
    if (bind(sockfd_, (sockaddr *)&my_addr, sizeof(sockaddr)) == -1)
      {
        printf("bind error\n");
        return -1;
      }
    printf("socket bound\n");

    printf("Velodyne socket fd is %d\n", sockfd_);

    return 0;
}

/*---------------------------Get Packets-----------------------------
 * Reads data from the UDP socket into buffer.
 * Npacks determines the number of packets to be read in
 * each packet is 1206 bytes long
 * make sure buffer is at least of size 1206*npacks
 * -------------------------------------------------------------------*/
int getPackets(uint8_t *buffer, int npacks)
{
 int result = npacks;
 //double time1 = ros::Time::now().toSec();
 //if we are not readding data from a file
	if(playback==0)
	{
	 struct pollfd fds[1];
	 fds[0].fd = sockfd_;
	 fds[0].events = POLLIN;
	 static const int POLL_TIMEOUT = 1000; // one second (in msec)

	 for (int i = 0; i < npacks; ++i)
	   {
		 // poll() until input available, this ensures we aren't trying to read data that isn't there
		 do
		 {
			 int retval = poll(fds, 1, POLL_TIMEOUT);
			 if (retval < 0)             // poll() error?
			   {
	//                 if (errno != EINTR)
				   printf("poll() error\n");
				 return -1;
			   }
			 if (retval == 0)            // poll() timeout?
			   {
				 printf("Velodyne poll() timeout\n");
				 return -1;
			   }
			 if ((fds[0].revents & POLLERR)
				 || (fds[0].revents & POLLHUP)
				 || (fds[0].revents & POLLNVAL)) // device error?
			   {
				 printf("poll() reports Velodyne error\n");
				 return -1;
			   }
		   } while ((fds[0].revents & POLLIN) == 0);
		// printf("polling complete\n");
		 //fflush(stdout);
		 // Read packets that should now be available from the socket.
		 ssize_t nbytes = recvfrom(sockfd_, &buffer[i * packet_size], packet_size,  0, NULL, NULL);
		 //printf("bytes recieved: %i\n",nbytes );
		 if ((size_t) nbytes == packet_size)
		   {
			 --result;
		   }
		 else
		   {
			 printf("incomplete Velodyne packet read: %i bytes", (int)nbytes);
			 --i;                        // rerun this loop iteration
		   }
	   }

   }
	else //if we are reading from a file
		for(int i = 0; i<npacks; ++i)
		{
			fread(&buffer[i * packet_size],1,1206,pfile);
			if(feof(pfile))
				return -1;
		}
 //double time2 = ros::Time::now().toSec();

 // Average the times at which we begin and end reading.  Use that to
 // estimate when the scan occurred.
 //*data_time = (time2 + time1) / 2.0;

 return 0;
}

/*---------------------------Read One Status-----------------------------
 * Reads a single packet and extracts the type and value variables from it.
 * Does NOT parse the buffer other then the type and value bytes
 * Used in Read Configuration function
 * -------------------------------------------------------------------*/
int read1Status(char* type, double* value)
{
	//reads 1 packet and extracts the status bytes
	//packet tempP;
	uint8_t Tbuffer[1206];
	if(getPackets(Tbuffer,1)!=-1)
	{
	//parseBuffer(Tbuffer,&tempP);
	//*type = (int)tempP.statType;
	//*value = (double)tempP.statVal;
	//dont think it is necessary to parse full buffer every time, leaving commented for stress test later
	//printf("Type: %c, Value: %c\n", (char)Tbuffer[1204], (char)Tbuffer[1205]);
		*type = Tbuffer[1204]; //1204 = location of the type byte in buffer
		*value =Tbuffer[1205]; //1205 = location of the value byte in buffer
		return 0;
	}
	else
		return -1;

}

/*---------------------------Read Configration-----------------------------
 * Read the necessary number of packets to obtain configuration data
 * Configuration data is stored in the conf structure for later use
 * -------------------------------------------------------------------*/
int readConfiguration(conf* config)
{
	//need type 1 and 2 because many of the parameters are spread over 2 messages
	char type1, type2;
	double value1, value2;
	//copy high and low block titles for printing purposes
	strcpy(config->highBlock.title,"high");
	strcpy(config->lowBlock.title,"low");
	//wait for beginning of a status sequence
	//the word UNITS is output when laser configuration data is about to begin
	printf("\n\nWAITING FOR U\n\n");
	do
		if(read1Status(&type1,&value1)==-1)
			return -1;
		while(type1!='1' || ((char)value1)!='U');
	//loop through all 64 lasers
	for(int i= 0; i<64; ++i)
	{
		//wait for the number of the laser you are about to read data for
		do
			if(read1Status(&type1,&value1)==-1)
				return -1;
			while(type1!='1');
		printf("\n\nLASER: %i\n\n",(int)value1);
		//if the laser is part of the lower block
		if(value1<32)
		{
			//store the value of the laser being read in //could probably just use i
			int lasernum =(int)value1;
			//read values for all laser configuration data
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].vertCorrection=(value2*256+value1)/100;
			if(config->lowBlock.laser[lasernum].vertCorrection>327.68)
				config->lowBlock.laser[lasernum].vertCorrection-=655.36;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].rotCorrection=(value2*256+value1)/100;
			if(config->lowBlock.laser[lasernum].rotCorrection>327.68)
				config->lowBlock.laser[lasernum].rotCorrection-=655.36;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].farCorrection=(value2*256+value1)/1000;
			//every 7 messages ther is time data output that we can skip
			do
				if(read1Status(&type1,&value1)==-1)
					return -1;
			while(type1!='1');
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].xCorrection=(value2*256+value1)/1000;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].yCorrection=(value2*256+value1)/1000;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].vOffCorrection=(value2*256+value1)/1000;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			do
				if(read1Status(&type2,&value2)==-1)
					return -1;
				while(type2!='1');
			config->lowBlock.laser[lasernum].hOffCorrection=(value2*256+value1)/1000;
			if(config->lowBlock.laser[lasernum].hOffCorrection>32.768)
							config->lowBlock.laser[lasernum].hOffCorrection-=65.536;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].focalDist=(value2*256+value1)/1000;
			if(config->lowBlock.laser[lasernum].focalDist>32.768)
							config->lowBlock.laser[lasernum].focalDist-=65.536;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].focalSlope=(value2*256+value1)/10;
			if(config->lowBlock.laser[lasernum].focalSlope>3276.8)
							config->lowBlock.laser[lasernum].focalSlope-=6553.6;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->lowBlock.laser[lasernum].minIntensity=(uint8_t)value1;
			config->lowBlock.laser[lasernum].maxIntensity=(uint8_t)value2;
		}
		else //if the laser is part of the upper block
		{
			int lasernum =(int)value1-32;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].vertCorrection=(value2*256+value1)/100;
			if(config->highBlock.laser[lasernum].vertCorrection>327.68)
					config->highBlock.laser[lasernum].vertCorrection-=655.36;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].rotCorrection=(value2*256+value1)/100;
			if(config->highBlock.laser[lasernum].rotCorrection>327.68)
					config->highBlock.laser[lasernum].rotCorrection-=655.36;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].farCorrection=(value2*256+value1)/1000;
			do
				if(read1Status(&type1,&value1)==-1)
					return -1;
				while(type1!='1');
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].xCorrection=(value2*256+value1)/1000;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].yCorrection=(value2*256+value1)/1000;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].vOffCorrection=(value2*256+value1)/1000;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			do
				if(read1Status(&type2,&value2)==-1)
					return -1;
				while(type2!='1');
			config->highBlock.laser[lasernum].hOffCorrection=(value2*256+value1)/1000;
			if(config->highBlock.laser[lasernum].hOffCorrection>32.768)
								config->highBlock.laser[lasernum].hOffCorrection-=65.536;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].focalDist=(value2*256+value1)/1000;
			if(config->highBlock.laser[lasernum].focalDist>32.768)
								config->highBlock.laser[lasernum].focalDist-=65.536;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].focalSlope=(value2*256+value1)/10;
			if(config->highBlock.laser[lasernum].focalSlope>3276.8)
								config->highBlock.laser[lasernum].focalSlope-=6553.6;
			if(read1Status(&type1,&value1)==-1)
				return -1;
			if(read1Status(&type2,&value2)==-1)
				return -1;
			config->highBlock.laser[lasernum].minIntensity=(uint8_t)value1;
			config->highBlock.laser[lasernum].maxIntensity=(uint8_t)value2;
		}
		//at the end of the configuration there is one more set of data that we don't care about and can skip
		do
			if(read1Status(&type1,&value1)==-1)
				return -1;
			while(type1!='1');
	}
	return 0;
}

/*---------------------------printLaserConfig-----------------------------
 * Used mostly for debugging or saving configuration data to a file
 * Prints a single laser configuration
 * block should point to either the high or low block of a conf struct
 * lasernum should be a number between 0 and 31
 * -------------------------------------------------------------------*/
void printLaserConfig(const blockconf* block, int lasernum)
{
	if(lasernum>=0 && lasernum<32)
	{
		printf("\n-In %s block-\n\n", block->title);
		printf("-Configuration Data for laser %i:\n", lasernum);
		printf("\tVertical Correction: %f\n", block->laser[lasernum].vertCorrection);
		printf("\tRotational Correction: %f\n", block->laser[lasernum].rotCorrection);
		printf("\tDistance Far Correction: %f\n", block->laser[lasernum].farCorrection);
		printf("\tDistance Correction X: %f\n", block->laser[lasernum].xCorrection);
		printf("\tDistance Correction Y: %f\n", block->laser[lasernum].yCorrection);
		printf("\tVertical Offset Correction: %f\n", block->laser[lasernum].vOffCorrection);
		printf("\tHorizontal Offset Correction: %f\n", block->laser[lasernum].hOffCorrection);
		printf("\tFocal Distance: %f\n", block->laser[lasernum].focalDist);
		printf("\tFocal Slope: %f\n", block->laser[lasernum].focalSlope);
		printf("\tMin Intensity: %i\n", block->laser[lasernum].minIntensity);
		printf("\tMax Intensity: %i\n", block->laser[lasernum].maxIntensity);
	}
	else
		printf("Invalid Laser Number\n");
}

/*---------------------------Process Packet-----------------------------
 * Takes a packet that has been parsed and processes it to apply the
 * configuration and convert the data to xyz coordinates. The data is then
 * stored into the file pointed to by dfile
 * pkt is a pointer to the packet you want to process
 * cfg is a pointer to the configuration you want to apply to it
 * dfile is a pointer to the file youwant to store data in
 * -------------------------------------------------------------------*/
int processPacket(const packet* pkt, conf* cfg)
{
	//define all variables used for calculation
	// x, y, zpos ar the final storage variables for the converted data
	// distance is the distance read from the sensor + the default overall distance correction (farCorrection)
	// heading in the rotation converted to radians and normalized
	// pitch comes from the vertical correction of the sensor
	// xy_proj is the projection of the distance value on the xy_plane
	// x_proj is the x value of the xy_proj vector used to calculate LIXcorrection
	// y_proj is the y value of the xy_proj vector used to calculate LIYcorrection
	// LIX and LITcorrection are the interpolated correction values for x and y
	// x1dist and y1dist are the corrected xy_proj for the x and y direction
	double xpos,ypos,zpos, distance, heading, pitch, xy_proj, x1dist, y1dist, x_proj, y_proj, LIXcorr, LIYcorr;
	double intensityScale, intensityVal, focalOffset;
	blockconf* block;
	for(int q = 0; q<12; ++q)
	{
		//determine if the block belongs to the lower or upper set of lasers
		 if(pkt->block[q].id==0xDDFF)
		 {
			 //printf("DDFF\n");
			 block = &(cfg->highBlock);
		 }
		 else
		 {
			 //printf("EEFF\n");
			 block = &(cfg->lowBlock);
		 }
		 for(int j = 0; j<32; ++j)
		 {
			 //ignore all readings closer than 1m
			 if(pkt->block[q].data[j].distance>1)
			 {
				 //------------------------xyz calculation----------------------------
				 //calulate default distance value
				 distance=pkt->block[q].data[j].distance+block->laser[j].farCorrection;
				 //calculate heading and apply rotation correction
				 heading=normalize_angle(-(from_degrees(pkt->block[q].rotation)-from_degrees(block->laser[j].rotCorrection)));
				 //calculate pitch in radians
				 pitch = from_degrees(block->laser[j].vertCorrection);
				 //calculate the projection on the xyplane
				 xy_proj = distance*cos(pitch);
				 //calculate default distances in x and y direction; sin and cos are opposite because the velodyne uses opposite x and y
				 x_proj = xy_proj*cos(heading) - block->laser[j].hOffCorrection*sin(heading);
				 y_proj = xy_proj*sin(heading) - block->laser[j].hOffCorrection*cos(heading);
				 //use linear interpolation to calculate correct distance corrections
				 //may need switch x and y corrections
				 if(x_proj < 25.04)
					 LIXcorr = block->laser[j].yCorrection+(block->laser[j].farCorrection-block->laser[j].yCorrection)*(x_proj-1.93/(25.04-1.93));
				 else
					 LIXcorr = block->laser[j].farCorrection;
				 if(y_proj < 25.04)
					 LIYcorr = block->laser[j].xCorrection+(block->laser[j].farCorrection-block->laser[j].xCorrection)*(y_proj-2.40/(25.04-2.40));
				 else
					 LIYcorr = block->laser[j].farCorrection;
				 //apply correct distance corrections instead of default distance and calculate xy_proj
				 x1dist = (pkt->block[q].data[j].distance+LIXcorr)*cos(pitch);
				 y1dist = (pkt->block[q].data[j].distance+LIYcorr)*cos(pitch);
				 //convert to x and y coordinates
				 xpos = x1dist*cos(heading) - block->laser[j].hOffCorrection*sin(heading);
				 ypos = y1dist*sin(heading) - block->laser[j].hOffCorrection*cos(heading);
				 //calulate z position, there is no special correction data for the z-axis so we use default correction
				 zpos = distance*sin(pitch) + block->laser[j].vOffCorrection;

				 //------------------------intensity calculation----------------------------
				 intensityScale= (double)(block->laser[j].maxIntensity - block->laser[j].minIntensity);
				 focalOffset = 256*(1-block->laser[j].focalDist)*(1-block->laser[j].focalDist)/(13100*13100);
				 intensityVal = (double)pkt->block[q].data[j].intensity+block->laser[j].focalSlope*(abs(focalOffset-256*(1-distance*500)*(1-distance*500)/((long double)65535*(long double)65535)));
				 if(intensityVal<block->laser[j].minIntensity) intensityVal=block->laser[j].minIntensity;
				 if(intensityVal>block->laser[j].maxIntensity) intensityVal=block->laser[j].maxIntensity;
				 intensityVal = (intensityVal - (double)block->laser[j].minIntensity)/intensityScale;
				 //print to file, extra data is simply for information and verification purposes, might want to switch to tabs delimiters to make easier to read to excel
				 //fprintf(dfile, "%f,%f,%f,%f,%f\n",intensityVal,xpos,ypos,zpos,pkt->block[q].rotation);
				 x[pointCount]=xpos;
				 y[pointCount]=ypos;
				 z[pointCount]=zpos;
				 intensity[pointCount]=intensityVal;
				 ++pointCount;
			 }
		  }
	}
	return 0;
}
